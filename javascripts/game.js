var game = new Phaser.Game(1024, 576, Phaser.AUTO, 'game', { preload: preload, create: create, update: update });

var bulletTime = 0,
    sbulletTime = 0,
    hbulletTime = 0,
    initialPlayerPosition = 512;
    lives = 4,
    score = 0,
    highScore = 0;
    level = 1;
    skill = 3;
    bulletnum = 20;
    vol = 0.5;
    mode = 1;
    bossalive = 1;
var style = { font: "32px silkscreen", fill: "#666666", align: "center" },
    boldStyle = { font: "bold 32px silkscreen", fill: "#ffffff", align: "center" };

function setupExplosion (explosion) {
  explosion.animations.add('explode');
}

function playerMovement () {
  var maxVelocity = 500;
  helper.body.velocity.x += (player.body.x-helper.body.x)/100;
  if (cursors.left.isDown && player.body.velocity.x > -maxVelocity) {
    // Move to the left
    player.body.velocity.x -= 20;
    // helper.body.velocity.x -= 20;
    player.play('left');
  }
  else if (cursors.right.isDown && player.body.velocity.x < maxVelocity) {
    // Move to the right
    player.body.velocity.x += 20;
    // helper.body.velocity.x += 20;
    player.play('right');
  }
  else {
    // Slow down
    if (player.body.velocity.x > 0) {
      player.body.velocity.x -= 4;
    }
    else if (player.body.velocity.x < 0) {
      player.body.velocity.x += 4;
    }
    player.play('up');
  }
}
var num;
function fireBullet (num) {
  bulletnum = num;
  if(game.time.now > sbulletTime) {
    sbullet = sbullets.getFirstExists(false);
    if (sbullet) {
      // And fire it
      if(mode == 1 && skill != 1){
        shootSound.play();
        sbullet.reset(player.x, player.y - 16);
        sbullet.body.velocity.y = -400;
        sbullet.body.velocity.x = player.body.velocity.x / 4
        sbulletTime = game.time.now + 400;
        // console.log("666666");
      }else if(skill == 1 && mode == 1){
        bombSound.play();
        sbullet.reset(player.x, player.y - 16);
        sbullet.body.velocity.y = -400;
        sbullet.body.velocity.x = player.body.velocity.x / 4
        sbulletTime = game.time.now + 200;
      }
      
    }
  }
  // if(game.time.now > hbulletTime) {
  //   hbullet = hbullets.getFirstExists(false);
  //   if (hbullet) {
  //     // And fire it
  //     if(score > 200){
  //       shootSound.play();
  //       hbullet.reset(player.x - 20, player.y - 16);
  //       hbullet.body.velocity.y = -400;
  //       hbullet.body.velocity.x = player.body.velocity.x / 4
  //       hbulletTime = game.time.now + 400;
  //       // console.log("666666");
  //     }else if(skill == 1){
  //       bombSound.play();
  //       hbullet.reset(player.x - 20, player.y - 16);
  //       hbullet.body.velocity.y = -400;
  //       hbullet.body.velocity.x = player.body.velocity.x / 4
  //       hbulletTime = game.time.now + 200;
  //     }
      
  //   }
  // }
  if (game.time.now > bulletTime) {
    bullet = bullets.getFirstExists(false);
    
    
    if (bullet) {
      // And fire it
      if(skill!=1){
        shootSound.play();
        bullet.reset(player.x, player.y - 16);
        bullet.body.velocity.y = -400;
        bullet.body.velocity.x = player.body.velocity.x / 4
        bulletTime = game.time.now + 400;
      }  
      else{
        shootSound.play();
        bullet.reset(player.x, player.y - 16);
        bullet.body.velocity.y = -400;
        bullet.body.velocity.x = player.body.velocity.x / 4
        bulletTime = game.time.now + 200;
      }
    }
  }
}
function fireBullet1 (num) {
  
  if (score > 100) {
    bullet = bullets.getFirstExists(false);
    
    
    if (bullet) {
      // And fire it
      if(skill!=1){
        shootSound.play();
        bullet.reset(player.x - 16, player.y - 16);
        bullet.body.velocity.y = -400;
        bullet.body.velocity.x = player.body.velocity.x / 4
        bulletTime = game.time.now + 400;
      }  
      else{
        shootSound.play();
        bullet.reset(player.x, player.y - 16);
        bullet.body.velocity.y = -400;
        bullet.body.velocity.x = player.body.velocity.x / 4
        bulletTime = game.time.now + 200;
      }
    }
  }
}
function bulletHitsAlien (bullet, alien) {
  this.emitter.x = bullet.x; 
  this.emitter.y = bullet.y;
  this.emitter.start(true, 800, null, 15);
  bullet.kill();
  explode(alien);
  var numalien = 0;
  if(bossalive == 0){
    if(numalien > 16){respawnboss();numalien = 0;bossalive = 1;}
    else numalien += 1;
    
  }
  if(mode == 1)score += 50;
  else score += 10;
  if(score > 500){
    if(bossalive == 1){
      explode1(boss);
      bossalive = 0;
    }
    // helper.body.x = player.body.x;
    mode = 1;
    
  }
  if(score > 100){
    mode = 1;
  }
  updateScore();
  if(score > level*15){
    level = level + 1;
  }
  if (aliens.countLiving() == 0) {
    newWave();
  }
}
function clean () {
  meunText.destroy();
  startText.destroy();
}

function bombHitsPlayer (bomb, player) {
  bomb.kill();
  explode(player);
  
  bulletnum = 5;
  this.emitter.x = player.x; 
  this.emitter.y = player.y;
  this.emitter.start(true, 800, null, 15);
  lives -= 1;
  skill -= 1;
  
  updateLivesText();
  if (lives > 0) {
    respawnPlayer();
    if(bossalive == 0){
      respawnboss();
      bossalive = 1;
    }
  }
  else {
    gameOver();
  }
}

function explode (entity) {

  entity.kill();

  // And create an explosion :)
  explodeSound.play();
 
  var explosion = explosions.getFirstExists(false);
  explosion.reset(entity.body.x + (entity.width / 2), entity.body.y + (entity.height / 2));
  explosion.play('explode', 30, false, true);
  
}
function explode1 (entity) {

  entity.kill();

  // And create an explosion :)
  explodeSound.play();
 
  var explosion = explosions.getFirstExists(false);
  explosion.reset(entity.body.x + (entity.width / 2), entity.body.y + (entity.height / 2));
  explosion.play('explode', 30, false, true);
  
}

function updateLivesText () {
  livesText.text = "LIVES: " + lives;
  skillText.text = "SKILL CD: " + skill;
  


  // gameOverText.text = "";
  // restartText.text = "";
}

function getHighScore () {
  savedHighScore = Cookies.get('highScore');
  if (savedHighScore != undefined) {
    highScore = savedHighScore;
  }
}

function updateScore () {
  if (score > highScore) {
    highScore = score;
  }
  levelText.text = "Level: " + level;
  scoreText.text = pad(score, 6);
  highScoreText.text = "HIGH: " + pad(highScore, 6);
}

function respawnPlayer () {
  player.body.x = initialPlayerPosition;
  // helper.body.x = initialPlayerPosition;
  setTimeout(function () {
    player.revive();
  }, 1000);
}

function respawnboss () {
  boss.body.x = initialPlayerPosition;
  // helper.body.x = initialPlayerPosition;
  setTimeout(function () {
    boss.revive();
  }, 1000);
}

function newWave () {
  setTimeout(function () {
    aliens.removeAll();
    createAliens();
    animateAliens();
  }, 1000);
}

function restartGame () {
  gameOverText.destroy();
  restartText.destroy();
  level = 1;
  lives = 3;
  mode = 0;
  skill = 3;
  score = 0;
  updateScore();
  updateLivesText();

  respawnPlayer();
  newWave();
}
function menu () {

  if(lives == 4){
    meunText = game.add.text(game.world.centerX, game.world.centerY, "Raiden", boldStyle);
    meunText.anchor.set(0.5, 0.5);
    startText = game.add.text(game.world.centerX, game.world.height - 16, "PRESS 'sssssssssssssss' to start", style);
    startText.anchor.set(0.5, 1);
    lives-=1;
  }else{

  }

}
function gameOver () {
  setTimeout(function() {
    gameOverText = game.add.text(game.world.centerX, game.world.centerY, "GAME OVER", boldStyle);
    gameOverText.anchor.set(0.5, 0.5);
    restartText = game.add.text(game.world.centerX, game.world.height - 16, "PRESS 'S' TO RESTART", style);
    restartText.anchor.set(0.5, 1);

    Cookies.set('highScore', highScore, { expires: '2078-12-31' });
  }, 1000);
}

function createAliens () {
  aliens = game.add.group();
  aliens.enableBody = true;
  aliens.physicsBodyType = Phaser.Physics.ARCADE;
  var z = 5;
  if(level + 5 > 10)z = 10;
  else z = z + level;
  if(score < 100000){
    for (var y = 0; y < 4; y++) {
      
      for (var x = 0; x < z; x++) {
        var alien = aliens.create(x * 72, y * 48, 'alien');
        alien.anchor.setTo(0.5, 0.5);
        alien.body.moves = false;
      }
    }
  }else{
    // for (var y = 0; y < 4; y++) {
    //   for (var x = 0; x < 10; x++) {
    //     var alien = aliens.create(x * 72, y * 48, 'alien');
    //     alien.anchor.setTo(0.5, 0.5);
    //     alien.body.moves = false;
    //   }
    // }
  }

  aliens.x = 64;
  aliens.y = 96;

  aliens.forEach(function (alien, i) {
    game.add.tween(alien).to( { y: alien.body.y + 5 }, 500, Phaser.Easing.Sinusoidal.InOut, true, game.rnd.integerInRange(0, 500), 1000, true);
  })
}
function soundup () {
  if(vol<=0.9){shootSound.volume += 0.1;
  explodeSound.volume += 0.1;
  bombSound.volume += 0.1;
  vol += 0.1;}
  volText.text = "vol: " + vol;
}
function sounddown () {
  if(vol>=0.1){shootSound.volume -= 0.1;
  explodeSound.volume -= 0.1;
  bombSound.volume -= 0.1;
  vol -= 0.1;}
  volText.text = "vol: " + vol;
}
function animateAliens () {
  // All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
  var tween = game.add.tween(aliens).to( { x: 308 }, 2500, Phaser.Easing.Sinusoidal.InOut, true, 0, 1000, true);

  // When the tween loops it calls descend
  tween.onLoop.add(descend, this);
}

function handleBombs () {
  aliens.forEachAlive(function (alien) {
    chanceOfDroppingBomb = game.rnd.integerInRange(0, (20 * aliens.countLiving()));
    if (chanceOfDroppingBomb == 0) {
      dropBomb(alien);
    }
  }, this)
}

function dropBomb (alien) {
  bomb = bombs.getFirstExists(false);

  if (bomb && player.alive) {

    bombSound.play();
    // And drop it
    bomb.reset(alien.x + aliens.x, alien.y + aliens.y + 16);
    bomb.body.velocity.y = +100 + 10*level;
    bomb.body.gravity.y = 250 + 10*level;
  }
}

function descend () {
  if (player.alive) {
    //aliens.y += 8;
    game.add.tween(aliens).to( { y: aliens.y + 8 }, 2500, Phaser.Easing.Linear.None, true, 0, 0, false);
  }
}

function pad(number, length) {
  var str = '' + number;
  while (str.length < length) {
    str = '0' + str;
  }
  return str;
}
