function create () {
  game.physics.startSystem(Phaser.Physics.ARCADE);
  starfield = game.add.tileSprite(0,0, 1000, 800, 'starfield');
  // Initialize player
  // player = game.add.sprite(initialPlayerPosition, 540, 'ship');
  
  player = game.add.sprite(512, 540, 'ship', 1);
  player.anchor.setTo(0.5, 0.5);
  player.smoothed = false;
  player.scale.set(4);
  game.physics.enable(player, Phaser.Physics.ARCADE);

  player.body.bounce.x = 0.5;
  player.body.collideWorldBounds = true;

  boss = game.add.sprite(512, 50, 'alien1', 1);
  boss.anchor.setTo(0.5, 0.5);
  boss.smoothed = false;
  boss.scale.set(1);
  game.physics.enable(boss, Phaser.Physics.ARCADE);

  boss.body.bounce.x = 0.5;
  boss.body.collideWorldBounds = true;

  helper = game.add.sprite(512, 540, 'boss', 1);
  helper.anchor.setTo(0.5, 0.5);
  helper.smoothed = false;
  helper.scale.set(0.2);
  game.physics.enable(helper, Phaser.Physics.ARCADE);

  helper.body.bounce.x = 0.5;
  helper.body.collideWorldBounds = true;
  //////////////////////////////////////////////
  var left;
  var right;
  var weapon;
  left = player.animations.add('left', [8,9], 10, true);
  right = player.animations.add('right', [1,2], 10, true);
  player.animations.add('up', [11,12,13], 10, true);
  player.animations.add('down', [4,5,6], 10, true);
  // Create the emitter with 15 particles. 
  this.emitter = game.add.emitter(0, 0, 15); 
  // Set the 'pixel' image for the particles 
  this.emitter.makeParticles('pixel');
  this.emitter.setYSpeed(-150, 150); 
  this.emitter.setXSpeed(-150, 150);
  this.emitter.setScale(2, 0, 2, 0, 800);
  this.emitter.gravity = 0;
  //////////////////////////////////////////////
  // Initialize bullets
  bullets = game.add.group();
  bullets.enableBody = true;
  bullets.physicsBodyType = Phaser.Physics.ARCADE;
  bullets.createMultiple(bulletnum, 'bullet');
  bullets.setAll('anchor.x', 0.5);
  bullets.setAll('anchor.y', 1);
  bullets.setAll('checkWorldBounds', true);
  bullets.setAll('outOfBoundsKill', true);
  bullets.bulletAngleVariance = 10;
  
  sbullets = game.add.group();
  sbullets.enableBody = true;
  sbullets.physicsBodyType = Phaser.Physics.ARCADE;
  sbullets.createMultiple(bulletnum, 'sbullet');
  sbullets.setAll('anchor.x', 0.5);
  sbullets.setAll('anchor.y', 1);
  sbullets.setAll('checkWorldBounds', true);
  sbullets.setAll('outOfBoundsKill', true);
  sbullets.bulletAngleVariance = 10;

  hbullets = game.add.group();
  hbullets.enableBody = true;
  hbullets.physicsBodyType = Phaser.Physics.ARCADE;
  hbullets.createMultiple(bulletnum, 'hbullet');
  hbullets.setAll('anchor.x', 0.5);
  hbullets.setAll('anchor.y', 1);
  hbullets.setAll('checkWorldBounds', true);
  hbullets.setAll('outOfBoundsKill', true);
  hbullets.bulletAngleVariance = 10;
  // weapon = game.add.weapon(30, 'bullet');

  // //  The bullet will be automatically killed when it leaves the world bounds
  // weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
  // //  Because our bullet is drawn facing up, we need to offset its rotation:
  // weapon.bulletAngleOffset = 90;
  // //  The speed at which the bullet is fired
  // weapon.bulletSpeed = 400;
  // //  Speed-up the rate of fire, allowing them to shoot 1 bullet every 60ms
  // weapon.fireRate = 60;
  // //  Add a variance to the bullet speed by +- this value
  // weapon.bulletSpeedVariance = 200;
  // weapon.trackSprite(sprite, 14, 0);
  

  // Initialize aliens
  createAliens();
  animateAliens();

  // Initialize bombs
  bombs = game.add.group();
  bombs.enableBody = true;
  bombs.physicsBodyType = Phaser.Physics.ARCADE;
  bombs.createMultiple(10, 'bomb');
  bombs.setAll('anchor.x', 0.5);
  bombs.setAll('anchor.y', 0.5);
  bombs.setAll('checkWorldBounds', true);
  bombs.setAll('outOfBoundsKill', true);

  // Initialize explosions
  explosions = game.add.group();
  explosions.createMultiple(10, 'explosion');
  explosions.setAll('anchor.x', 0.5);
  explosions.setAll('anchor.y', 0.5);
  explosions.forEach(setupExplosion, this);

  // Text bits
  
  livesText = game.add.text(game.world.bounds.width - 16, 16, "LIVES: " + lives, style);
  livesText.anchor.set(1, 0);
  skillText = game.add.text(game.world.bounds.width - 16, 50, "SKILL: " + skill, style);
  skillText.anchor.set(1, 0);
  volText = game.add.text(game.world.bounds.width - 16, 84, "VOL: " + vol, style);
  volText.anchor.set(1, 0);  
  scoreText = game.add.text(game.world.centerX, 16, '', style);
  scoreText.anchor.set(0.5, 0);

  levelText = game.add.text(16,160,'', style);
  levelText.anchor.set(0, 0);
  highScoreText = game.add.text(16, 16, '', style);
  highScoreText.anchor.set(0, 0);

  getHighScore();

  updateScore();

  // Initialize sounds
  shootSound = game.add.audio('shoot', 1, false);
  explodeSound = game.add.audio('explode', 1, false);
  bombSound = game.add.audio('bomb', 1, false);

  // Setup controls
  cursors = game.input.keyboard.createCursorKeys();
  fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  restartButton = game.input.keyboard.addKey(Phaser.Keyboard.S);
  skillButton = game.input.keyboard.addKey(Phaser.Keyboard.C);
  soundupButton = game.input.keyboard.addKey(Phaser.Keyboard.Q);
  sounddownButton = game.input.keyboard.addKey(Phaser.Keyboard.W);
}
