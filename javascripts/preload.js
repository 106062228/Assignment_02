function preload () {
  // game.load.image('ship', 'images/ship.png');
  // game.load.image('ship', 'images/spaceman.png');
  game.load.image('bullet', 'images/bullet.png');
  game.load.image('sbullet', 'images/sbullet.png');
  game.load.image('hbullet', 'images/bullet.png');
  game.load.image('pixel', 'images/pixel.png');
  game.load.image('alien', 'images/alien.png');
  game.load.image('alien1', 'images/alien1.png');
  game.load.image('boss', 'images/boss.png');
  game.load.image('bomb', 'images/bomb.png');
  game.load.image('starfield', 'images/starfield.png');
  game.load.spritesheet('explosion', 'images/explosion.png', 80, 80);
  /////////////////////////////
  game.load.spritesheet('ship', 'images/ship.png', 16, 16);
  ////////////////////////////
  game.load.audio('shoot', 'sounds/shoot.wav');
  game.load.audio('explode', 'sounds/explode.wav');
  game.load.audio('bomb', 'sounds/bomb.wav');
}
