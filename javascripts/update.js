var gamestart = 0;
function update () {
  if(gamestart == 0 && !restartButton.isDown){
    menu();
  }
  
  else{
    if(restartButton.isDown && gamestart == 0){
      // gameOverText.destroy();
      clean();
      console.log("1111111111");
    }
    playerMovement();
    gamestart = 1;
    // Firing?
    if (fireButton.isDown && player.alive) {
      fireBullet(5);
      fireBullet1(20);
    }
    if(skillButton.isDown && player.alive && skill > 0){
      fireBullet(20);
      // fireBullet1(20);
    }
    // Restart?
    if (restartButton.isDown && lives == 0) {
      restartGame();
    }
    if(soundupButton.isDown){
      soundup();
      // console.log("11111111");
    }
    if(sounddownButton.isDown){
      sounddown();
    }
    starfield.tilePosition.y += 2;
    // Handle aliens dropping bombs
    handleBombs();

    game.physics.arcade.overlap(bullets, aliens, bulletHitsAlien, null, this);
    game.physics.arcade.overlap(bombs, player, bombHitsPlayer, null, this);
  }
}
